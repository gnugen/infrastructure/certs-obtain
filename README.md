# Certificate management

## Update an existing cert
```sh
vim requests/example.com.ini
make
```

# To create a new certificate

Generate a private key
```sh
openssl genrsa 4096 > keys/example.com.key
```

Edit the function `push_in_git` in `lib/deploy.sh` to declare on which
host the certificate will be deployed and then run:

```sh
make
```

# To get a new certificate for a domain (force renewal)

If for some reason you need to get a new certificate for a domain (say the
current certificate is invalid), you can `touch requests/example.com.ini`
and `make` to get a new one.
