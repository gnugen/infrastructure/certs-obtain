#!/bin/bash

RENEW_WHEN_REMAIN_DAYS="10"

main ()
{
	for cert in ./certificates/*.crt; do
		[ "$cert" = "./certificates/*.crt" ] && return
		check_certificate "$cert"
		if [ "$remaining_days" -le "$RENEW_WHEN_REMAIN_DAYS" ]; then
			local name="$(basename "${cert%.*}")"
			local ini="./requests/$name.ini"
			echo "Invalidating $ini for old age"
			[ -f "$ini" ] && touch -c "$ini"
		fi
		local errcode=$?
		if [ $errcode -ne 0 ]; then
			return $errcode
		fi
	done
}

warn ()
{
	printf "%s\n" "$*" >&2
}

check_certificate ()
{
	local cert="$1"
	remaining_days="0"
	if [ -e "$cert" ]
	then
		if ! openssl_output="$(openssl x509 -noout -in "$cert" -enddate)"
		then
			warn "openssl wasn't able to get the expiration date. Considering the $cert as expired/invalid."
			remaining_days=0
			return
		fi
		local expire_date="$(echo "$openssl_output" | cut -d= -f2-)"
		local expire_ts="$(date -d "$expire_date" +%s)"
		remaining_days="$(( (expire_ts - $(date +%s)) / 86400))"
	fi
}

main
