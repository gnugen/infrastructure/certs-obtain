#!/bin/sh

ACCOUNT_KEY="./keys/account.key"
cmd="$0"

main ()
{
	if [ $# -ne 2 ] || [ -z "$1" ] || [ -z "$2" ]; then
		warn "$command: invalid usage."
		usage
		exit 1
	fi

	csr="$1"
	cert="$2"

	python3 ./acme-tiny/acme_tiny.py \
		--account-key "$ACCOUNT_KEY" \
		--csr "$csr" \
		--acme-dir ./www \
		--disable-check \
		> "$cert"
	ret="$?"

	if [ ! "$ret" = "0" ]
	then
		warn "acme-tiny returned $ret. There was most likely a problem in obtaining the certificate."
	fi

	if [ ! -e "$cert" ]
	then
		warn "acme-tiny didn't create the certificate."
		return 126
	fi

	if [ "$(stat --format '%s' "$cert")" = "0" ]
	then
		warn "acme-tiny created an empty certificate"
		# Remove it to ensure we try again during the next run
		rm "$cert"
		return 127
	fi

	return "$ret"
}

usage() {
	warn "  $command CERT SOURCE_FILE"
	warn "This script will touch \$SOURCE_FILE if \$CERT is older than $RENEW_WHEN_REMAIN_DAYS days"
}

warn() {
	printf "%s\n" "$*" >&2
}

main "$@"
