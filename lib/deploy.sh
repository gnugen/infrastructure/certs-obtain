#!/bin/sh

main() {
	for f in ./certificates/*.crt; do
		local d
		d="$(basename "$f")"
		d="${d%.*}"
		key=keys/"$d".key
		cert=certificates/"$d".crt
		push_in_git "$d"
	done

	for d in ./git/*; do
		[ -d "$d" ] || continue
		[ "$d" = "./git/template" ] && continue
		push_chain_in_git "$(basename "$d")"
	done
}

warn() {
	printf "%s\n" "$*" >&2
}

push_in_git() {
	case "$1" in
	bristen.gnugen.ch)
		push_in_git_for_host bristen
		;;
	rapperswil.gnugen.ch)
		push_in_git_for_host rapperswil
		;;
	rainbowdash.gnugen.ch)
		push_in_git_for_host rainbowdash
		;;
	rapperswil.gnugen.ch)
		push_in_git_for_host rapperswil
		;;
	morgarten.gnugen.ch)
		push_in_git_for_host morgarten
		;;
	petit-ruan.gnugen.ch)
		push_in_git_for_host petit-ruan
		;;
	*.petit-ruan.gnugen.ch)
		push_in_git_for_host petit-ruan
		;;
	matrix-unipoly) #TODO: kill it
		push_in_git_for_host petit-ruan
		;;
	conode.gnugen.ch)
		push_in_git_for_host petit-ruan
		;;
	*)
		warn "I don't know where to push the certificate for $1."
		;;
	esac
}

git_prepare() {
	if [ ! -d "git/$1" ]
	then
		warn "Repository for host $1 is missing."
		return 1
	fi

	(
	cd "git/$1" || return 1
		if [ -n "$(git status --porcelain)" ]
		then
			git add .
			git commit -m "uncommited changes before update" >/dev/null
		fi
	)
}

git_finish() {
	(
	cd "git/$1" || return 1
	[ -z "$(git status --porcelain)" ] && return  # nothing changed
	git add . || warn "git add . failed in repository for $1"
	git commit -m "$2" >/dev/null \
		|| warn "git commit failed in repository for $1"
	git push  >/dev/null || warn "git push failed in repository for $1"
	)
}

push_chain_in_git() {
	[ -z "$1" ] && return
	git_prepare "$1" || return

	cp ca/isrgrootx1.pem "git/$1/"
	cp ca/dstrootcax3.pem "git/$1/"
	cp ca/lets-encrypt-x1-cross-signed.pem "git/$1/"
	cp ca/lets-encrypt-x3-cross-signed.pem "git/$1/"
	cp ca/lets-encrypt-x4-cross-signed.pem "git/$1/"
	cp ca/lets-encrypt-r3.pem "git/$1/"

	git_finish "$1" "Update certificate chain"
}

push_in_git_for_host() {
	[ -z "$1" ] && return

	git_prepare "$1" || return

	mkdir -p   "git/$1/certificates/"
	cmp "$cert" "git/$1/certificates/$(basename "$cert")" \
		|| cp "$cert" "git/$1/certificates/"
	mkdir -p   "git/$1/keys/"
	cmp "$key" "git/$1/keys/$(basename "$key")" \
		|| cp "$key"  "git/$1/keys/"

	git_finish "$1" "Update for $domain"
}

main
