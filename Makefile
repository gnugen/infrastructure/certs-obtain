SHELL := /bin/bash
ifeq ($(SILENT), )
	DEBUG_ECHO="echo"
else
	DEBUG_ECHO=":"
endif

common_name_bristen.gnugen.ch = bristen.gnugen.ch
common_name_rainbowdash.gnugen.ch = rainbowdash.gnugen.ch
common_name_rapperswil.gnugen.ch = rapperswil.gnugen.ch
common_name_morgarten.gnugen.ch = morgarten.gnugen.ch
common_name_matrix-unipoly = petit-ruan.gnugen.ch
common_name_conode.gnugen.ch = conode.gnugen.ch
common_name_web-dispatch.petit-ruan.gnugen.ch = petit-ruan.gnugen.ch

# input
requests := $(wildcard requests/*.ini)
# output
desired_certs := $(patsubst requests/%.ini, certificates/%.crt, $(requests))

existing_certs := $(wildcard certificates/*.crt)


all: deprecate-old-certs $(desired_certs) deploy


# check if the existing certificates are getting old
# this will `touch` the source ini for the old certs, so that they get built again
deprecate-old-certs:
	@$(DEBUG_ECHO) "Deprecating old certs"
	@lib/deprecate_old_certs.sh
.PHONY: deprecate-old-certs


# print a warning if a key doesn't exist
keys/%.key:
	@echo "$@ is missing"
	@echo "if this isn't a mistake, you could create one with openssl genrsa 4096"
	@exit 1


# generate the certificate request from the ini file
requests/object/%.csr: requests/%.ini keys/%.key /etc/ssl/openssl.cnf
	@mkdir -pv requests/object/
	@if [ -z "$(common_name_$*)" ]; then \
		echo "Please define common_name_$* in the Makefile"; \
		exit 1; \
	fi
	@$(DEBUG_ECHO) "Generating the request (csr)"
	@openssl req -new -sha256 -key keys/$*.key \
	       	-subj "/CN=$(common_name_$*)" \
		-reqexts SAN \
		-config <(cat /etc/ssl/openssl.cnf $<) \
		> $@


# ask let's encrypt for the certificate give a csr
certificates/%.crt: requests/object/%.csr
	@mkdir -pv certificates/
	@$(DEBUG_ECHO) "Asking let's encrypt for $<"
	@lib/get_certificate.sh "$<" "$@"


# push the new certificates to production
deploy:
	@$(DEBUG_ECHO) "Deploying"
	@lib/deploy.sh
.PHONY: deploy


clean:
	rm -r requests/object/*.csr
	rm -r certificates/*.crt
.PHONY: clean
